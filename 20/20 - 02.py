def fun():
    return '5' in [input()[-1] for i in range(int(input()))]

n = int(input())
print(('НЕТ', 'ДА')[sum([fun() for i in range(n)]) == n])