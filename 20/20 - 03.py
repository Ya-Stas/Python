import sys
x = sys.stdin.readlines()
sum = 0
l = 0
if len(x) == 0:
    print("-1")
else:
    for j in range(0, len(x)):
        sum += int(x[j])
        l += 1
    answer = sum / l
    print(answer)