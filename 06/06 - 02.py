n = int(input())
used = set()
for i in range(n):
    town = input()
    used.add(town)
line = input()
if line in used:
    print('TRY ANOTHER')
else:
    print('OK')