M = int(input())
N = int(input())
if M <= 0 or N <= 0:
    print('ОШИБКА')
else:
    bibl = set()
    for i in range(M):
        knig = input()
        bibl.add(knig)
    for i in range(N):
        knig1 = input()
        if knig1 in bibl:
            print('YES')
        else:
            print('NO')