number = buf = int(input())
letter = 'A'
for i in range(number*number):
    if (i+1) % number != 0:
        print(chr(ord(letter)), buf, sep='', end=' ')
        letter = chr(ord(letter)+1)
    else:
        print(chr(ord(letter)), buf, sep='')
        letter = 'A'
        buf -= 1