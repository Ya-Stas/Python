text = input()
legal = {"_"}
for i in range(ord("a"), ord("z") + 1):
    legal.add(chr(i))
for char in text:
    if char not in legal:
        print('Неверный символ,', char)
        break