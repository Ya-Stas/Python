number = int(input())
text = input()
remember = ('.', '?', '!', ':', ';', '-', '—', ' ')
res = ''
for i in range(len(text)):
    if text[i] not in remember:
        res += chr(ord(text[i])+number)
    else:
        res += text[i]
print(res)