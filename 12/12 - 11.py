for i in range(int(input())):
    a = input().split()
    n = a.index(min(a))
    if (all(a[i] >= a[i + 1] for i in range(n))
            and all(a[i] <= a[i + 1] for i in range(n, len(a) - 1))):
        a.sort(reverse=True)
        print(' '.join(a))
    else:
        print('НЕТ')