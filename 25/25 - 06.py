import pymorphy2
import sys

morph = pymorphy2.MorphAnalyzer()
string = sys.stdin.read().split()
life = morph.parse('живой')[1]

for i in string:
    i = morph.parse(i)[0]

    if i.tag.number == 'sing' and 'NOUN' in i.tag:
        if i.tag.animacy == 'anim':
            print(life.inflect({str(i.tag.gender)}).word.capitalize())
        else:
            print(f"Не {life.inflect({str(i.tag.gender)}).word}")
    elif i.tag.number == 'plur' and 'NOUN' in i.tag:
        if i.tag.animacy == 'anim':
            print(life.inflect({str(i.tag.number)}).word.capitalize())
        else:
            print(f"Не {life.inflect({str(i.tag.number)}).word}")
    else:
        print('Не существительное')