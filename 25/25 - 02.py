import pymorphy2
from sys import stdin
import re

morph = pymorphy2.MorphAnalyzer()

text = list(map(str.rstrip, stdin))
buf = ' '.join(text)
reg = re.compile("[^a-zA-Zа-яА-Я]")
stroke = reg.sub(' ', buf).lower().split()

words = {}

for i in stroke:
    p = morph.parse(i)[0]
    if p.score > 0.5 and 'NOUN' in p.tag:
        words[p.normal_form] = words.get(p.normal_form, 0) + 1
res = [x[0] for x in sorted(words.items(), key=lambda x: (x[1], x[0]), reverse=True)]

print(*res[:10])