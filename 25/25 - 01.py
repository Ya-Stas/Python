import pymorphy2
from sys import stdin

def count_word(string, words):
    crt = 0
    morph = pymorphy2.MorphAnalyzer()
    letter = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя \n'
    word = ''.join([i for i in string if i in letter]).split()
    for i in word:
        if morph.parse(i)[0].normal_form in words:
            crt += 1
    return crt

print(count_word(stdin.read().lower(), ['видеть', 'увидеть', 'глядеть', 'примечать', 'узреть']))