N = int(input())
average = int(0)
for i in range(1, N+1):
    IQ = int(input())
    if IQ > average and i != 1:
        print('>')
    elif IQ == average or i == 1:
        print('0')
    else:
        print('<')
    average = int((average + IQ) / i)