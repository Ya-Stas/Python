s1 = ['а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х',
        'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']
s2 = ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х',
        'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я']

def encrypt_caesar(msg, shift):
    res = ""
    for i in msg:
        if i in s1:
            ind = s1.index(i) % len(s1)
            res += s1[(ind + shift) % len(s1)]
        elif i in s2:
            ind = s2.index(i) % len(s1)
            res += s2[(ind + shift) % len(s1)]
        else:
            res += i
    return res

def decrypt_caesar(msg, shift):
    res = ""
    for i in msg:
        if i in s1:
            ind = s1.index(i)
            res += s1[ind - shift]
        elif i in s2:
            ind = s2.index(i)
            res += s2[ind - shift]
        else:
            res += i
    return res
