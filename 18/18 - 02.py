def letter(name, pl, d, email):
    return f'''To: {email}
Здравствуйте, {name}!
Были бы рады видеть вас на встрече начинающих программистов в {pl}е, которая пройдет {d}.'''


place = 'Нефтекамск'
name = 'Олег'
date = '31.12.21'
email = 'Oleg@mail.ru'
print(letter(name, place, date, email))
name = 'Евгений'
place = 'Нефтекамск'
date = '31.12.21'
email = 'evgeniy@mail.ru'
print(letter(name, place, date, email))