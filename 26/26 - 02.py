import random
from pptx import Presentation
from pptx.util import Pt
prs = Presentation()


for method in dir(random):
    if not method.startswith('_'):
        text = eval(f'random.{method}.__doc__')
        if text:
            slide = prs.slides.add_slide(prs.slide_layouts[1])
            slide.shapes.title.text = method
            text_frame = slide.placeholders[1].text_frame
            p = text_frame.paragraphs[0]
            run = p.add_run()
            run.text = text
            font = run.font
            font.name = 'Courier New'
            font.size = Pt(12)
            font.bold = True
            font.italic = None
prs.save('res.pptx')