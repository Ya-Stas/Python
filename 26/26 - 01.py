from docx import Document
from sys import stdin

place = input('Место проведения:')
time = input('Время проведения:')
print('ФИО одноклассниц')
names = []
crt = 0
for i in stdin:
    names.append(i.rstrip())
doc = Document()
for i in names:
    doc.add_heading('Приглашение', 0)
    doc.add_heading('Дорогая ' + i + ', приглашаем тебя на мероприятие посвящённое празднику 8 марта')
    doc.add_paragraph('В ' + place)
    doc.add_paragraph('В ' + time)
    if crt < len(names)-1:
        doc.add_page_break()
        crt += 1
doc.save('invite.docx')

