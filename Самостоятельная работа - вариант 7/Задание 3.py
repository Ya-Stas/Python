number = int(input()) * 2
fmin = 0
fmax = 0
res = ''
buf = ''
for i in range(number):
    text = input().split()
    if (i+1) % 2 != 0:
        fmin = text.index(min(text, key=int))
        fmax = text.index(max(text, key=int))
        for y in range(len(text)):
            if (y >= fmin) and (y <= fmax):
                res += text[y] + ' '
        buf = text
    else:
        num = (int(min(buf, key=int)) + int(max(buf, key=int))) / 2
        for y in range(len(text)):
            if int(text[y]) > num:
                res += text[y] + ' '
    res += '\n'
print(res)