def discriminant(a, b, c):
    return b * b - 4 * a * c

def larger_root(p, q):
    d = discriminant(1, p, q)
    return (-p + d ** 0.5) / 2

def smaller_root(p, q):
    d = discriminant(1, p, q)
    return (-q + d ** 0.5)

def main():
    print(smaller_root(2, 1))
    print(larger_root(2, 1))
main()