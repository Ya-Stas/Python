st = {}
for i in range(int(input())):
    words = input().split()
    word = words[0]
    st[word] = words[1:len(words) + 1]
for i in range(int(input())):
    word = input()
    if word not in st:
        print('Нет в словаре')
    else:
        print(*st[word])