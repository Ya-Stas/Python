n = int(input())
p = [list(map(int,input().split())) for _ in range(n)]
s = dict()
for i in range(n):
    a = p[i][0] // 10, p[i][1] // 10
    if a not in s:
        s[a] = 0
    s[a] += 1
print(max(s.values()))