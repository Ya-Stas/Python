count = 0
stroke = ''
buf = 0
kot = 0
while stroke != 'СТОП':
    stroke = input()
    count += 1
    if 'кот' in stroke or 'Кот' in stroke:
        kot += 1
        if buf != 0:
            continue
        buf = count
if buf != 0:
    print(kot, buf, end='')
else:
    print(kot, -1, end='')