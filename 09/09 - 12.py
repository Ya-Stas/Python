s = input()
n = int(s[:s.index(' ')])
total = int(s[s.index(' '):])
errors, true_total = [], 0
for i in range(n):
    primer = input()
    price = int(primer[:primer.index('*')])
    amount = int(primer[primer.index('*')+1:primer.index('=')])
    cost = int(primer[primer.index('=')+1:])
    if price * amount != cost:
        errors.append(i+1)
    true_total += price * amount
print(total - true_total)
for x in errors:
    print(x, end=' ')