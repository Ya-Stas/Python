import random
planets = ['Меркурий', 'Венера', 'Земля', 'Марс', 'Юпитер', 'Сатурн', 'Уран', 'Нептун']
planet = random.choice(planets)
print('Какую планету я загадал?')
answer = input()
answer = planet
if answer == 'Плутон':
    print('Плутон уже не считается планетой.')
elif answer not in planets:
    print('Это вообще не планета.')
elif answer == planet:
    print('Да, правильный ответ:', planet)
else:
    print('Неверно.')
input()